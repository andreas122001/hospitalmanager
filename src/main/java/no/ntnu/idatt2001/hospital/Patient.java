package no.ntnu.idatt2001.hospital;

/**
 * This class defines a patient-object. A patient is a persons, but with a diagnosis.
 *
 * @author andrebw
 */
public class Patient extends Person implements Diagnosable{

    private String diagnosis = ""; //the patients diagnosis

    /**
     * Constructor calls supers constructor. Diagnosis is left out here, since that is up to a doctor to set.
     *
     * @param firstName persons first name
     * @param lastName persons last name
     * @param socialSecurityNumber persons social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName,lastName,socialSecurityNumber);
    }

    /**
     * Since diagnosis is protected we don't want to show the diagnosis in the toString.
     * Only calls the supers toString.
     *
     * @return format: 'firstName, lastName; socialSecurityNumber'
     */
    @Override
    public String toString() {
        return super.toString();
    }

    //getter and setter for diagnosis:
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    //This method is protected, so only classes in the same package can access it.
    protected String getDiagnosis(){
        return diagnosis;
    }
}
