package no.ntnu.idatt2001.hospital;

import no.ntnu.idatt2001.hospital.exception.RemoveException;
import no.ntnu.idatt2001.hospital.healthpersonnel.doctor.Surgeon;

import java.util.Scanner;

/**
 * This is just a class for testing.
 */
public class Testing {
    /*
    public static void main(String[] args) {
        Surgeon doctor = new Surgeon("Pål", "Andersen", "80574");
        Patient patient = new Patient("Jon", "Isaac","80453");
        doctor.setDiagnosis(patient, "Fatal familial insomnia");
        System.out.println(patient.toString() + " " + patient.getDiagnosis());
    }
    */
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println(hospital.toString());
        System.out.println("         ^^^ Before removing ^^^");
        Scanner sc = new Scanner(System.in);
        System.out.print("Type anything to remove employees...");
        String input = sc.nextLine();
        System.out.println("\n\n");

        System.out.println("REMOVING EMPLOYEE 1");
        try {
            hospital.getDepartments().get(0).remove(new Employee("Odd Even", "Primtallet", "1"));
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        System.out.println("EMPLOYEE REMOVED\n");
        System.out.println("REMOVING EMPLOYEE 2");
        try {
            hospital.getDepartments().get(0).remove(new Patient("Kåre","Jan", "2"));
        } catch (RemoveException e) {
            System.out.println(e.getMessage() + "\n");
        }

        System.out.println("         ᐯᐯᐯ After removing ᐯᐯᐯ");
        System.out.println(hospital.toString());
        sc.close();
    }
}
