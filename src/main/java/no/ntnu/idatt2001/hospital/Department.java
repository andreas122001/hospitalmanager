package no.ntnu.idatt2001.hospital;

import no.ntnu.idatt2001.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * This class deals with employees and patients at a department of a hospital.
 * @author andrebw
 */
public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Constructor that creates a department-object.
     * @param departmentName name of the department.
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    /**
     * Removes a patient from the patients list if it exists.
     * @param patient to be removed.
     * @throws RemoveException if patient is not found in list. Returns the SSN of the patient.
     */
    public void remove(Patient patient) throws RemoveException {
        if (!patients.contains(patient)) {
            throw new RemoveException("Patient: " + patient.getSocialSecurityNumber());
        } else {
            patients.remove(patient);
        }
    }

    /**
     * Removes an employee from the employees list if it exists.
     * @param employee to be removed.
     * @throws RemoveException if employee is not found in list. Returns the SSN of the employee.
     */
    public void remove(Employee employee) throws RemoveException {
        if (!employees.contains(employee)) {
            throw new RemoveException("Employee: " + employee.getSocialSecurityNumber());
        } else {
            employees.remove(employee);
        }
    }

    /**
     * Returns a string with all the relevant content of the department-object.
     * @return department string.
     */
    @Override
    public String toString() {
        return "      " + departmentName + ": " + employees.size() + " ansatte, " + patients.size() + " pasienter\n"
                + getEmployeesString() + "\n" + getPatientsString();
    }

    /**
     * @return the string of all patients in the list.
     */
    public String getPatientsString() {
        String text = "         Pasienter:";
        for (Patient p : patients) {
            text += "\n             " + p.toString();
        }
        return text;
    }

    /**
     * @return the string of all employees in the list.
     */
    public String getEmployeesString() {
        String text = "         Ansatte:";
        for (Employee e : employees) {
            text += "\n              " + e.toString();
        }
        return text;
    }

    /**
     * Adds an employee to the employee-list if an employee with the same SSN does not exist.
     * @param employee employee to add.
     * @throws IllegalArgumentException if employee already exists.
     */
    public void addEmployee(Employee employee) throws IllegalArgumentException {
        if (!(employees.contains(employee))) {
            employees.add(employee);
        } else {
            throw new IllegalArgumentException(String.format("Could not add: employee with SSN '%s' already exists",
                    employee.getSocialSecurityNumber()));
        }
    }

    /**
     * Adds a patient to the patient-list if a patient with the same SSN does not exist.
     * @param patient to be added.
     * @throws IllegalArgumentException if patient already exists.
     */
    public void addPatient(Patient patient) throws IllegalArgumentException {
        if (!patients.contains(patient)) {
            patients.add(patient);
        } else {
            throw new IllegalArgumentException(String.format("Could not add: patient with SSN '%s' already exists", patient.getSocialSecurityNumber()));
        }
    }

    //Getters and setters:
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    //auto-generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    //auto-generated
    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
}
