package no.ntnu.idatt2001.hospital;

/**
 * A class that defines an employee.
 */
public class Employee extends Person {

    /**
     * Same constructor as super.
     *
     * @param firstName persons first name
     * @param lastName persons last name
     * @param socialSecurityNumber persons social security number
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    //Same as super.
    @Override
    public String toString() {
        return super.toString();
    }
}
