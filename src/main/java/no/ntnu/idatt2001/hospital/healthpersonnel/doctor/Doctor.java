package no.ntnu.idatt2001.hospital.healthpersonnel.doctor;

import no.ntnu.idatt2001.hospital.Employee;
import no.ntnu.idatt2001.hospital.Patient;

/**
 * Defines the standard doctor object type
 */
public abstract class Doctor extends Employee {

    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName,lastName,socialSecurityNumber);
    }

    abstract void setDiagnosis(Patient patient, String diagnosis);
}
