package no.ntnu.idatt2001.hospital.healthpersonnel;

import no.ntnu.idatt2001.hospital.Employee;

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
