package no.ntnu.idatt2001.hospital.exception;

/**
 * An exception-class for when removing something fails.
 * @author andrebw
 */
public class RemoveException extends Exception {
    public RemoveException (String message) {
        super("Could not remove: " + message + " not found");
    }
}
