package no.ntnu.idatt2001.hospital;

import java.util.Objects;

/**
 * A class that defines the default behaviour and data of all person-classes.
 *
 * @author andrebw
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private final String socialSecurityNumber;

    /**
     * The constructor creates an instance of an object 'Person'. Fails if SSN is not a number.
     *
     * @param firstName persons first name
     * @param lastName persons last name
     * @param socialSecurityNumber persons social security number
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        try {
            Integer.parseInt(socialSecurityNumber);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("SSN must be a number");
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return full name and social security number
     */
    @Override
    public String toString() {
        return getFullName() + "; " + getSocialSecurityNumber();
    }

    /**
     * Gets the full name of the person in the format 'lastName, firstName'
     *
     * @return full name of the person
     */
    public String getFullName(){
        return getLastName() + ", " + getFirstName();
    }


    //getters and setters:
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /** (auto-generated)
     * Checks if a object equals this classes object. The check is based on the social security number, which is
     * unique for every person. First and last name is not included since two persons can share the same name.
     *
     * @param o object to be checked
     * @return true if it is equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    /** (auto-generated)
     * The hashCode is, as the equals-method, based on the social security number.
     *
     * @return hash code for the object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
