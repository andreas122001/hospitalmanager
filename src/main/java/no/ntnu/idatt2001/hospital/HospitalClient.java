package no.ntnu.idatt2001.hospital;

import no.ntnu.idatt2001.hospital.exception.RemoveException;
import static javax.swing.JOptionPane.*;

/**
 * A client to run the HospitalManager.
 *
 * NOTE: Jeg var litt usikker på hva oppgaveteksten mente i oppgave 5, men dette er altså slik jeg har forstått det.
 * I denne klienten kan du fjerne ansatt, fjerne pasient eller printe info, men av pasienter og ansatte kan du ikke
 * velge hvilke som blir fjernet, for enkelhets skyld. Dette er altså bare en enkel test-klient for å teste
 * remove-metoden. Om du prøver å fjerne en ansatt fungerer det første gangen, men å fjerne en pasient vil feile med
 * en gang, som beskrevet i oppgaven.
 * Klassen Testing fjerner to personer uten input fra bruker og demonstrerer også at remove-metoden fungerer.
 */
public class HospitalClient {
    static Hospital hospital = new Hospital("St. Olavs Hospital");
    public static void main(String[] args) {
        HospitalTestData.fillRegisterWithTestData(hospital);

        boolean execute = true;
        while(execute) {
            String[] options = {"Fjern ansatt", "Fjern pasient", "Print info", "Avslutt"};
            int input = showOptionDialog(null, "Hva vil du gjøre?", hospital.getHospitalName(), DEFAULT_OPTION,
                    INFORMATION_MESSAGE,null,options,options[0]);
            switch (input){
                case 0:
                    try {
                        hospital.getDepartments().get(0).remove(new Employee("","","1"));
                    } catch (RemoveException e) {
                        showMessageDialog(null,"Det skjedde en feil!\n" + e.getMessage());
                        break;
                    }
                    showMessageDialog(null,"Ansatt fjernet!");
                    break;

                case 1:
                    try {
                        hospital.getDepartments().get(0).remove(new Patient("","","1"));
                    } catch (RemoveException e) {
                        showMessageDialog(null,"Det skjedde en feil!\n" + e.getMessage());
                        break;
                    }
                    showMessageDialog(null,"Pasient fjernet!");
                    break;

                case 2:
                    showMessageDialog(null, hospital.toString());
                    break;

                default:
                    execute = false;
            }
        }
    }
}
