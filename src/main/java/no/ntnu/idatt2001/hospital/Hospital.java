package no.ntnu.idatt2001.hospital;

import java.util.ArrayList;

/**
 * This class defines a hospital and deals with the departments in said hospital.
 */
public class Hospital {

    final private String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Constructor that creates a hospital-object.
     * @param hospitalName name of the hospital
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    /**
     * Returns the name of the hospital and the toString of all departments.
     * @return string of hospital.
     */
    @Override
    public String toString() {
        return getHospitalName() + getDepartmentsString() + "\n";
    }

    /**
     * Gets the toString of all departments in the list and returns them in one string.
     * @return to toString of all departments
     */
    public String getDepartmentsString() {
        String text = "";
        for (Department d : departments) {
            text += "\n" + d.toString() + "\n";
        }
        return text;
    }

    /**
     * Adds a department to the department-list if it doesn't already exist.
     * @param department to be added to the list.
     * @return true if successful, false if not.
     */
    public boolean addDepartment(Department department) {
        if (!departments.contains(department)) {
            departments.add(department);
            return true;
        } else {
            return false;
        }
    }

    //Getters and setters:
    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }
}
