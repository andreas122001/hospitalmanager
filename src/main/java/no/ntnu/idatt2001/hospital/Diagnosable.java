package no.ntnu.idatt2001.hospital;

/**
 * An interface for diagnosable classes.
 *
 * @author andrebw
 */
public interface Diagnosable {
    /**
     * Sets the diagnosis for the object.
     * @param diagnosis objects diagnosis
     */
    void setDiagnosis(String diagnosis);
}
