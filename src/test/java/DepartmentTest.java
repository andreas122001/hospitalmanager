import no.ntnu.idatt2001.hospital.Department;
import no.ntnu.idatt2001.hospital.Employee;
import no.ntnu.idatt2001.hospital.exception.RemoveException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.idatt2001.hospital.Patient;

public class DepartmentTest {

    private static Department department = new Department("Test department");
    private final PatientTest paalIvar = new PatientTest("Pål", "Ivar", "123456");
    private final PatientTest janTore = new PatientTest("Jan", "Tore", "654321");
    private final Employee kaareRugaard = new Employee("Kåre", "Rugård", "823212");
    private final Employee nilsAre = new Employee("Nils", "Are", "934323");

    @BeforeEach
    public void setup() {
        department = new Department("Test department");
    }

    public void addPatients() {
        department.addPatient(paalIvar);
        department.addPatient(janTore);
    }

    public void addEmployees() {
        department.addEmployee(kaareRugaard);
        department.addEmployee(nilsAre);
    }

    @Test
    @DisplayName("Non-existent patient can't be removed")
    public void canNotRemovePatientThatDoesNotExist() {
        assertThrows(RemoveException.class, () -> department.remove(new PatientTest("Arne", "Johansen", "828282")));
    }

    @Test
    @DisplayName("Other patient is not removed")
    public void correctPatientRemoved() {
        addPatients();
        assertTrue(department.getPatients().contains(paalIvar));
        assertTrue(department.getPatients().contains(janTore));
        try {
            department.remove(paalIvar);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        assertTrue(department.getPatients().contains(janTore));
        assertFalse(department.getPatients().contains(paalIvar));
    }

    @Test
    @DisplayName("Can also remove other patient")
    public void otherPatientRemoved() {
        addPatients();
        assertTrue(department.getPatients().contains(paalIvar));
        assertTrue(department.getPatients().contains(janTore));
        try {
            department.remove(janTore);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        assertTrue(department.getPatients().contains(paalIvar));
        assertFalse(department.getPatients().contains(janTore));
    }

    @Test
    @DisplayName("Removed patient doesn't exist")
    public void removedPatientDoesNotExist() {
        addPatients();
        try {
            department.remove(paalIvar);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        assertFalse(department.getPatients().contains(paalIvar));
    }

    @Test
    @DisplayName("Adding an already existing patient results in error")
    public void canNotAddAlreadyAddedPatient() {
        addPatients();
        assertThrows(IllegalArgumentException.class, () -> department.addPatient(paalIvar));
    }
}

/**
 * A test-class created to bypass the protected access of the Patient-constructor in the JUnit-tests
 */
class PatientTest extends Patient {
    protected PatientTest(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}



